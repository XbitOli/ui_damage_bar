using UnityEngine;
using UnityEngine.Events;

public class HealthModel : MonoBehaviour
{
    public UnityAction HealthChanged;

    [SerializeField] private float _health = 100f;

    private float _maxHealth = 100f;
    private float _minHealth = 0f;

    public float Health
    {
        get
        {
            return _health;
        }
        private set
        {
            _health = Mathf.Clamp(value, MinHealth, MaxHealth);
        }
    }

    public float MaxHealth { get => _maxHealth; private set => _maxHealth = value; }
    public float MinHealth { get => _minHealth; private set => _minHealth = value; }

    public void TakeDamage(float damage)
    {
        Health -= damage;
        HealthChanged?.Invoke();
    }

    public void Heal(float heal)
    {
        Health += heal;
        HealthChanged?.Invoke();
    }
}
