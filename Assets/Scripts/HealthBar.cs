using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(HealthModel))]
public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private float _animationSpeed;

    private Coroutine _barChanger;
    private HealthModel _health;

    private void OnEnable()
    {
        _health = GetComponent<HealthModel>();
        _health.HealthChanged += OnHealthChanged;
        _slider.maxValue = _health.MaxHealth;
        _slider.minValue = _health.MinHealth;
        _slider.value = _health.Health;                
    }

    private void OnDisable()
    {
        _health.HealthChanged -= OnHealthChanged;
    }

    private void OnHealthChanged()
    {
        if (_barChanger != null)
            StopCoroutine(_barChanger);

        _barChanger = StartCoroutine(ChangeSliderValue(_health.Health));
    }

    private IEnumerator ChangeSliderValue(float target)   
    {
        while (_slider.value != target)
        {
            _slider.value = Mathf.MoveTowards(_slider.value, target, _animationSpeed * Time.deltaTime);
            yield return null;
        }
    }
}
